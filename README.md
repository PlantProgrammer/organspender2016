# The ORGANizer (working title was Organspender2016)
One day you receive a tempting offer on your pager. ORGANize a liver and bathe in riches. You don't have a liver at hand? Just kindly "ask" one of the other patients then. What are you waiting for???

## Team LiverLovers
- Maximilian Reiß
- Artjom Fransen
- Gina Vörde
- Jonas Zimmer
- Simon Schliesky

## Builds
Available under https://plantandplay.itch.io/the-organizer
