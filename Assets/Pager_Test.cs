﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pager_Test : MonoBehaviour
{
    public List<string> events;

    int iterator;

    public void Start()
    {
        iterator = 0;
    }

    public void OnClick()
    {
        EventManager.Instance.Event("PAGER", "Display", events[iterator]);
        iterator++;
        if (iterator >= events.Count)
            iterator = 0;
    }
}