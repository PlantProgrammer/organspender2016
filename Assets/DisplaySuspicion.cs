﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplaySuspicion : MonoBehaviour
{
    public GameObject hideObject;
    public Slider slider;

    void Start()
    {
        EventManager.Instance.RegisterEventListener(
            "SUSPICION", ProcessSuspicionEvent);
        DisplaySlider(0);
    }

    protected void ProcessSuspicionEvent(string eventType, string param)
    {
        if (eventType != "Display") return;
        float _v = 0;
        if (!float.TryParse(param, out _v)) return;

        DisplaySlider(_v);
    }

    protected void Update()
    {
        if (GameManager.Instance.gameState == GameManager.GameStates.RUNNING)
            hideObject.SetActive(true);
        else
            hideObject.SetActive(false);
    }

    protected void DisplaySlider(float v)
    {
        slider.value = v;
        //gameObject.SetActive(v != 0);
    }
}
