﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PatientController : MonoBehaviour {

	public enum OrganTypes {HEART, LIVER, KIDNEY, LUNG};

	public OrganTypes[] organTypes;
	public bool hasBlanket = true;
    public string patientName = "Harald";

    public GameObject PatientNamePrefab;
    protected PatientName pName;

	public GameObject blanketStraight;
	public GameObject blanketCrumpled;

    GameObject Blanket;
	List<OrganTypes> organs;

	void OnTriggerEnter(Collider other) {
		Debug.Log("ENTER");
		if(other.tag == "Player") {
			PlayerController pc = PlayerController.Instance();
			bool patientHasMissionOrgan = organs.Contains(GameManager.Instance.missionOrgan);
			bool playerHoldsOrgan = pc.hasOrgan;
			bool patientIsTarget = (this.gameObject.Equals(GameManager.Instance.missionTarget));
			if((playerHoldsOrgan && patientIsTarget) 
				|| (patientHasMissionOrgan && !playerHoldsOrgan && !patientIsTarget)) {
				pc.canInteract = true;
				pc.interactionTarget = this.gameObject;
				hasBlanket = false;
			}
		}
	}

	void OnTriggerExit(Collider other) {
		if(other.tag == "Player") {
			Debug.Log("LEAVE");
			PlayerController.Instance().canInteract = false;
            hasBlanket = true;
		}
	}

	void Awake() {
		//Blanket = transform.Find("Blanket").gameObject;
		organs = new List<OrganTypes>();
		foreach (var organ in organTypes) {
			organs.Add(organ);
		}
        GameObject obj = (GameObject) GameObject.Instantiate(PatientNamePrefab);
        obj.transform.SetParent(gameObject.transform);
        obj.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        pName = obj.GetComponent<PatientName>();
        pName.Init(this);
	}

	void Update() {
		blanketStraight.SetActive(hasBlanket);
		blanketCrumpled.SetActive(!hasBlanket);
	}

	public void receiveOrgan(OrganTypes organ) {
		organs.Add(organ);
	}

	public void donateOrgan(OrganTypes organ) {
		organs.Remove(organ);
	}

	public void loseOrgan(OrganTypes organ) {
		organs.Remove(organ);
	}

    public bool hasOrgan(OrganTypes organ) {
        return organs.Contains(organ);            
    }
}
