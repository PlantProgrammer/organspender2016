﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PatientName : MonoBehaviour
{
    public TextMesh textContainer;
    public PatientController target;
    public Vector3 positionOffset = Vector3.up;

    public void Init(PatientController _target)
    {
        target = _target;
    }
    
	void Update ()
    {
        textContainer.text = target.patientName;
        transform.position = target.gameObject.transform.position + positionOffset;
        //textContainer.transform.position = Camera.main.WorldToViewportPoint(target.gameObject.transform.position + positionOffset);
    }
}
