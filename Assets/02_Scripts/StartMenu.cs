﻿using UnityEngine;
using System.Collections;

public class StartMenu : MonoBehaviour {
	public float interval;
	public float messageInterval;
	float timePassed;
	float buttonPressed;
	bool knowsHowToWalk = false;
	bool knowsHowToPressE = false;
	public enum Zones {EMPTY, START, EXIT, CREDITS, ONLINE}
	public Zones activeZone;
	public GameObject levelPosition;
	public GameObject startPosition;
	public GameObject jailPosition;
	public Pager pager;
	public GameObject playerStart;
	// Use this for initialization
	void Start () {
		timePassed = 0;
		activeZone = Zones.EMPTY;
	}
	
	// Update is called once per frame
	void Update () {
		timePassed += Time.deltaTime;
		if(timePassed >= messageInterval) {
			timePassed = 0;
			if(!knowsHowToWalk)
				EventManager.Instance.Event("PAGER", "Display", "Move around with W/A/S/D");
			if(knowsHowToWalk && !knowsHowToPressE)
				EventManager.Instance.Event("PAGER", "Display", "Press and hold E to activate");
		}
		if(!knowsHowToWalk && (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))) {
			knowsHowToWalk = true;
		}
		if(Input.GetButtonDown("Interact")) {
			knowsHowToPressE = true;
			buttonPressed = 0;
		}
		if(Input.GetButton("Interact")) {
			buttonPressed += Time.deltaTime;
		}
		if(Input.GetButtonUp("Interact")) {
			buttonPressed = 0;
		}
		if(buttonPressed >= interval ) {
			switch(activeZone) {
			case Zones.START:
                {
                    GameManager.Instance.gameState = GameManager.GameStates.RUNNING;
                    transform.position = playerStart.transform.position;
					Camera.main.transform.position = levelPosition.transform.position;
					EventManager.Instance.Event("MISSION", "New");
					pager.gameTime = 0;
					break;
				}
			case Zones.EXIT: {
					EventManager.Instance.Event("PAGER", "Display", "BYE BYE");
					Application.Quit();
					break;
				}
			case Zones.CREDITS: {
					GameManager.Instance.Credits();
					break;
				}
			case Zones.ONLINE: {
					Application.OpenURL("https://www.organspende-info.de/");
					break;
				}
			default:
			case Zones.EMPTY: {
					break;
					
				}
			}
			buttonPressed = 0;
			activeZone = Zones.EMPTY;
		}
	}

	void OnTriggerEnter(Collider other) {
		if(other.name == "StartCollider") {
			activeZone = Zones.START;
		} else if(other.name == "ExitCollider") {
			activeZone = Zones.EXIT;
		} else if(other.name == "CreditsCollider") {
			activeZone = Zones.CREDITS;
		} else if(other.name == "OnlineCollider") {
			activeZone = Zones.ONLINE;
		}
	}
	void OnTriggerExit(Collider other) {
		activeZone = Zones.EMPTY;
	}
}
