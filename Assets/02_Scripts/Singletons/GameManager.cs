﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager{
	// Instance
	private static GameManager _instance;
	public static GameManager Instance
	{
		get
		{
			if (_instance == null)
				_instance = new GameManager();
			return _instance;
		}
	}

	public enum GameStates {INIT, START, RUNNING, OVER};
	public GameStates gameState;
	private GameObject _player;
	public Quaternion cameraRotation;
	public GameObject[] Waypoints;
	GameManager() {
		gameState = GameStates.START;
		cameraRotation = Camera.main.transform.rotation;
		EventManager.Instance.RegisterEventListener("MISSION", ProcessMissionEvent);
		EventManager.Instance.RegisterEventListener("LOST", LoseGame);
    }

	public GameObject Player() {
		if(_player == null) {
			_player = GameObject.FindGameObjectWithTag("Player");
		}
		return _player;
	}

    protected List<GameObject> patients;
    protected List<GameObject> Patients
    {
        get { if (patients == null) CreatePatientList(); return patients; }
    }
    protected void CreatePatientList()
    {
        patients = new List<GameObject>();
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("patient"))
            patients.Add(o);
    }

    protected void WinGame()
    {
        //TODO Add winning stuff
        Debug.LogError("GAME WON GAME WON GAME WON");
    }

	protected void LoseGame(string eventType, string param = "") {
		PlayerController pc = PlayerController.Instance();
		pc.transform.position = pc.jailPlayerPosition.transform.position;
		Camera.main.transform.position = pc.jailPosition.transform.position;
        gameState = GameStates.OVER;
	}

    public void Credits()
    {
        List<string> credits = new List<string>();
        credits.Add("Artsy Fartsy: 61n4");
        credits.Add("Wireframe Chef Architekt: Auf zum Atom");
        credits.Add("Intelligente Pixel: Mac Similian");
        credits.Add("Nutellabroetchen und Code: Sim to the Som");
		credits.Add("KZH: Jonas");
		credits.Add("Music: Professor Umlaut by Kevin MacLeod (CC-BY 3.0)");
		

        foreach (string c in credits)
            EventManager.Instance.Event("PAGER", "Display", c);
    }

    public GameObject missionTarget; 
	public int missionValue;
    public PatientController.OrganTypes missionOrgan;

    protected void ProcessMissionEvent(string eventType, string param = "")
    {
        if (eventType == "New")
            CreateMission();
        if (eventType == "Accomplished")
        {
            EventManager.Instance.Event(
                "ACCOUNT", "Add", missionValue.ToString());
            CreateMission();
        }
        if (eventType == "GameOver")
        {
            WinGame();
        }

    }

    protected void CreateMission()
    {
        missionTarget = GetPatient();
        missionOrgan = GetTargetOrgan();
        missionValue = CalculateNewMissionValue();
        CommunicateNewMission();
    }


    List<int> lastPatients;
    List<int> LastPatients
    {
        get { if (lastPatients == null) lastPatients = new List<int>(); return lastPatients; }
    }
    /// <summary>
    /// Checks the last patients and returns a new target that's not equals the last two
    /// </summary>
    /// <returns>new target patient</returns>
    protected GameObject GetPatient()
    {
        int _target;
        for(;;)
        {
            _target = Random.Range(0, Patients.Count-1);
            if (LastPatients.Count != 0)
                if (_target == LastPatients[LastPatients.Count - 1])
                    continue;
            /*if (LastPatients.Count >= 2)
                if (_target == LastPatients[LastPatients.Count - 2])
                    continue;
             */
            break;
        }
        LastPatients.Add(_target);
        
        return Patients[_target];
    }

    /// <summary>
    /// Return a new MissionOrgan. Checks wether the organ is still available somewhere in the scene and removes it from the target patient.
    /// </summary>
    /// <returns></returns>
    protected PatientController.OrganTypes GetTargetOrgan()
    {
        PatientController.OrganTypes _target;
        for(;;)
        {
            _target = (PatientController.OrganTypes) Random.Range(0, 3);
            if (CheckIfOrganIsAvailable(_target, missionTarget))
                break;
        }
        DeleteOrganFromPatientIfAvailable(missionTarget, _target);
        return _target;
    }

    protected bool CheckIfOrganIsAvailable(PatientController.OrganTypes organToCheck, GameObject excludePatient = null)
    {
        foreach(GameObject o in Patients)
        {
            if (o == excludePatient)
                continue;
            PatientController pc =
                o.GetComponent<PatientController>();
            if (pc == null)
                continue;
            if (pc.hasOrgan(organToCheck))
                return true;
        }
        return false;
    }

    protected void DeleteOrganFromPatientIfAvailable(GameObject patient, PatientController.OrganTypes organToRemove)
    {
        PatientController pc =
               patient.GetComponent<PatientController>();
        if (pc == null)
        {
            Debug.LogError("GameManager::DeleteOrganFromPatientIfAvailable(), Mission Target has no PatientController");
            return;
        }
        if (pc.hasOrgan(organToRemove))
            pc.loseOrgan(organToRemove);
    }

    protected int CalculateNewMissionValue()
    {
        float _value = 0;
        switch (missionOrgan)
        {
            case PatientController.OrganTypes.HEART:
                _value = 160000;
                break;
            case PatientController.OrganTypes.KIDNEY:
                _value = 80000;
                break;
            case PatientController.OrganTypes.LIVER:
                _value = 120000;
                break;
            default:
            case PatientController.OrganTypes.LUNG:
                _value = 140000;
                break;
        }
        float _percent =
            1 + (Random.Range(-0.1f, 0.1f));
        _value *= _percent;
        return (int)_value;
    }

    protected void CommunicateNewMission()
    {
        PatientController pc =
               missionTarget.GetComponent<PatientController>();
        if (pc == null) return;
   
        string message = pc.patientName + ": Get me a new " + GetMissionLabel() + " (" + missionValue.ToString() + " $)";
        EventManager.Instance.Event(
            "PAGER", "Display", message);
        string information = "A " + GetMissionLabel() + " for " + pc.patientName;
        EventManager.Instance.Event(
            "MISSION", "Info", information);
    }

    protected string GetMissionLabel()
    {
        switch (missionOrgan)
        {
            case PatientController.OrganTypes.HEART:
                return "Heart";
            case PatientController.OrganTypes.KIDNEY:
                return "Kidney";
            case PatientController.OrganTypes.LIVER:
                return "Liver";
            default:
            case PatientController.OrganTypes.LUNG:
                return "Lung";
        }
    }
}
