﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Use case:
/// OnEnable: RegisterEventListener(string ID of event, method)
/// OnDisable: RemoveEventListener(string ID of event, method)
/// Call: Event(event ID, sub type, parameters if needed)
/// Example: EventManager.Instance.Event("PAGER", "Display", "Get me a liver");
/// </summary>
public class EventManager
{
    protected static EventManager instance;
    public static EventManager Instance
    {
        get
        {
            if (instance == null)
                instance = new EventManager();
            return instance;
        }
    }

    public delegate void EventMethod(string eventType, string param);
    protected Dictionary<string, List<EventMethod>> eventListeners;
    protected Dictionary<string, List<EventMethod>> EventListeners
    {
        get
        {
            if (eventListeners == null)
                eventListeners = new Dictionary<string, List<EventMethod>>();
            return eventListeners;
        }
        set { eventListeners = value; }
    }


    public void RegisterEventListener(string eventID, EventMethod listenerMethod)
    {
        if (!EventListeners.ContainsKey(eventID))
            EventListeners.Add(eventID, new List<EventMethod>());
        EventListeners[eventID].Add(listenerMethod);
    }

    public void RemoveEventListener(string eventID, EventMethod listenerMethod)
    {
        if (!EventListeners.ContainsKey(eventID))
            return;
        EventListeners[eventID].Remove(listenerMethod);
    }

    public void Event(string eventID, string eventType, string param = "")
    {
        Debug.Log("EventManager::Event, " + eventID + ": " + eventType + " (" + param + ")");
        if (!EventListeners.ContainsKey(eventID))
        {
            Debug.LogWarning("No event Listener for EventID " + eventID);
            return;
        }

        foreach (EventMethod e in EventListeners[eventID])
            e(eventType, param);
    }
}
