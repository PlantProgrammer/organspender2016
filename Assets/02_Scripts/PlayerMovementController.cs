﻿using UnityEngine;
using System.Collections;

public class PlayerMovementController : MonoBehaviour {

	GameObject player;
	// Use this for initialization
	void Start () {
		player = GameManager.Instance.Player();
	}

	public float movementSpeed = 1.5f;
	public Animator playeranim;

	// Update is called once per frame
	void Update () {
		if(GameManager.Instance.gameState == GameManager.GameStates.RUNNING ||
            GameManager.Instance.gameState == GameManager.GameStates.START ||
            GameManager.Instance.gameState == GameManager.GameStates.OVER) {
			move(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		}
	}

	void move(float haxis, float vaxis) {
		if(player) {
			Vector3 movementDirection = new Vector3(haxis,0,vaxis);
			if(movementDirection.magnitude >= 0.05f) {
				player.GetComponent<Animator>().SetBool("isWalking", true);
				// normalized direction of axis movement (world-space)
				movementDirection = movementDirection.normalized;
				// Copy rotation of camera to movement vector
				movementDirection = GameManager.Instance.cameraRotation * movementDirection;
				// Apply speed
				movementDirection = movementDirection * Time.deltaTime * movementSpeed;
				player.transform.LookAt(transform.position + movementDirection);
				player.transform.Translate(movementDirection,Space.World);
			} else {
				player.GetComponent<Animator>().SetBool("isWalking", false);
			}

		}
	}
}
