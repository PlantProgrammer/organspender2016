﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
	private static PlayerController _instance;
	public static PlayerController Instance() {
/*		if(_instance == null) {
			_instance = new PlayerController();
		} */
		return _instance;
	}

	public bool hasOrgan;
	public bool canInteract;
	public GameObject interactionTarget;
	PatientController.OrganTypes interactionOrgan;
	public GameObject jailPosition;
	public GameObject jailPlayerPosition;
	public GameObject startMenuPosition;
	private float buttonTimePressed;
	public float buttonTimeThreshold = 1.5f;
	private bool hasInteracted;

	public float restartTime = 1.5f;
	float restartTimer;
	// Use this for initialization
	void Start ()
    {
		restartTimer = 0;
        _instance = this;
		buttonTimePressed = 0;
		hasInteracted = false;
		Camera.main.transform.position = startMenuPosition.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonUp("Interact")) {
			buttonTimePressed = 0;
			hasInteracted = false;
		}
		if(canInteract) {
			if(Input.GetButtonDown("Interact") && !hasInteracted) {
				buttonTimePressed += Time.deltaTime;
				if(buttonTimePressed >= buttonTimeThreshold) {
					Interact();
					hasInteracted = true;
				}
			}
		}
		if(Input.GetKey(KeyCode.R)) {
			restartTimer += Time.deltaTime;
		} else {
			restartTimer = 0;
		}
		if(restartTimer >= restartTime) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
	}
	void Interact() {
		canInteract = false;
		PatientController patc = interactionTarget.GetComponent<PatientController>();
		if(hasOrgan) {
			patc.receiveOrgan(interactionOrgan);
			hasOrgan = false;
			gameObject.GetComponent<Animator>().SetInteger("inventory", 0);
			EventManager.Instance.Event("MISSION", "Accomplished");
			DoctorChanger.Instance.changeToIndex(0);
		} else {
			patc.donateOrgan(GameManager.Instance.missionOrgan);
			hasOrgan = true;
			interactionOrgan = GameManager.Instance.missionOrgan;
			gameObject.GetComponent<Animator>().SetInteger("inventory", 1);
			DoctorChanger.Instance.changeTo(GameManager.Instance.missionOrgan);
		}
	}

}
