﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Pager : MonoBehaviour
{
    public Text textContainer;
    public Text clockContainer;
    public RectTransform mask;

    protected enum PagerState { isHidden, isMovingUp, initDisplay, isDisplaying, isWaitingForNextText, isMovingDown }
    protected PagerState state;

    public float gameTime;

    protected float timeToMove = 0.5f;
    protected float movePerSecond = 500;
    protected float timeToWaitBetweenMessages = 1.0f;
    protected Vector3 startPosition;
    protected Vector3 endPosition;

    protected Vector3 textStartPosition;

    protected float distanceToMove = 132;

	AudioSource audio;
    protected List<string> textsToDisplay;
    protected List<string> TextsToDisplay
    {
        get { if (textsToDisplay == null) textsToDisplay = new List<string>(); return textsToDisplay; }
        set { textsToDisplay = value; }
    }

	void Awake() {
		audio = GetComponent<AudioSource>();
	}
    protected void OnEnable()
    {
        EventManager.Instance.RegisterEventListener("PAGER", ProcessPagerEvent);
        startPosition = transform.localPosition;
        endPosition = startPosition + new Vector3(0, distanceToMove);
        textStartPosition = new Vector3 (0, 0);
        deltaTime = 0;
    }

    protected void OnDisable()
    {
        EventManager.Instance.RemoveEventListener("PAGER", ProcessPagerEvent);
    }

    protected void ProcessPagerEvent(string eventType, string param="")
    {
        if (eventType == "Display")
        {
			audio.Play();
            if (state == PagerState.isHidden)
                state = PagerState.isMovingUp;
            TextsToDisplay.Add(param);
        }
    }

    protected void Update()
    {
        gameTime += Time.deltaTime;
        switch (state)
        {
            case PagerState.isHidden: return;
            case PagerState.isMovingUp:
                Move(true);
                break;
            case PagerState.isMovingDown:
                Move(false);
                break;
            case PagerState.initDisplay:
                InitDisplay();
                break;
            case PagerState.isDisplaying:
                Display();
                break;
            case PagerState.isWaitingForNextText:
                WaitForNextText();
                break;
        }
        if (clockContainer != null)
            clockContainer.text =
                ((int)(gameTime / 60)).ToString("00") + ":" +
                ((int)(gameTime % 60)).ToString("00");
            //System.DateTime.Now.Hour.ToString("00") + ":" + System.DateTime.Now.Minute.ToString("00");
    }

    float deltaTime;
    protected void Move(bool moveUp)
    {
        float v = deltaTime / timeToMove;
        if (v > 1) v = 1;
        if (moveUp)
            transform.localPosition =
                Vector3.Lerp(startPosition, endPosition, v);
        else
            transform.localPosition =
                Vector3.Lerp(endPosition, startPosition, v);
        if (v>=1)
        {
            if (moveUp)
                state = PagerState.initDisplay;
            else
                state = PagerState.isHidden;
            deltaTime = 0;
            return;
        }
        deltaTime += Time.deltaTime;
    }

    protected void InitDisplay()
    {
        if (textsToDisplay.Count == 0)
        {
            state = PagerState.isMovingDown;
            return;
        }
        textContainer.text = textsToDisplay[0];
        textsToDisplay.RemoveAt(0);
        deltaTime = 0;
        curHorizontalPosition = 0;
        state = PagerState.isDisplaying;
    }
    
    float curHorizontalPosition;
    protected void Display()
    {
        curHorizontalPosition += Time.deltaTime * movePerSecond;
        textContainer.gameObject.transform.localPosition =
            textStartPosition - new Vector3(curHorizontalPosition, 0);
           /* Vector3.Lerp(
                textStartPosition,
                textStartPosition - new Vector3(1600 + textContainer.rectTransform.sizeDelta.x, 0, 0),
                v);*/

        deltaTime += Time.deltaTime;
        if (curHorizontalPosition > 1600 + textContainer.rectTransform.sizeDelta.x)
        {
            if (TextsToDisplay.Count > 0)
                state = PagerState.isWaitingForNextText;
            else
                state = PagerState.isMovingDown;
            deltaTime = 0;
            textContainer.gameObject.transform.localPosition =
                textStartPosition;
            textContainer.text = "";
        }
    }

    protected void WaitForNextText()
    {
        if (deltaTime >= timeToWaitBetweenMessages)
            state = PagerState.initDisplay;
        deltaTime += Time.deltaTime;
    }
}
