﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Account : MonoBehaviour
{
    public Text textAccount;
    public int balance;
    protected int balanceToWinTheGame = 1000000;
	AudioSource audio;
	// Use this for initialization
	void Start ()
    {
        EventManager.Instance.RegisterEventListener(
            "ACCOUNT", ProcessAccountEvent);
        balance = 0;
        DisplayText();
		audio = GetComponent<AudioSource>();
    }
	
	protected void ProcessAccountEvent(string eventType, string param)
    {
        int _v = 0;
        if (!int.TryParse(param, out _v)) return;
        balance += _v;
		audio.Play();
        DisplayText();
        if (balance >= balanceToWinTheGame)
            EventManager.Instance.Event("MISSION", "GameOver");
    }

    protected void DisplayText()
    {
        textAccount.text = balance.ToString() + "$";
    }

    protected void Update()
    {
        if (GameManager.Instance.gameState == GameManager.GameStates.RUNNING)
            textAccount.gameObject.SetActive(true);
        else
            textAccount.gameObject.SetActive(false);
    }
}
