﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour
{
    public Waypoint[] neighbours;
//-----------------------------------------------------------------//
                //INITIALIZATION
//-----------------------------------------------------------------//

//-----------------------------------------------------------------//
                //DEFAULT METHODS
//-----------------------------------------------------------------//
    void Start ()
    {
	
	}
	void Update ()
    {
	
	}

//-----------------------------------------------------------------//
                //HELPER
//-----------------------------------------------------------------//
    public Waypoint GetRandomNeighbour(Waypoint wp)
    {
        if (neighbours.Length > 0)
        {
            int randIndex = Random.Range(0, neighbours.Length);
            if (wp != null)
            {
                if (neighbours[randIndex].GetInstanceID() == wp.GetInstanceID())
                {
                    randIndex += 1;
                    randIndex = randIndex % neighbours.Length;
                }  
            }
            return neighbours[randIndex];
        }
        else
        {
            Debug.Log("The Waypoint '" + this.name + "' doesn't has any neighbours");
            return null;
        }
    }
    public int GetNeighboursAmount()
    {
        return neighbours.Length;
    }
}
