﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIGuard : MonoBehaviour 
{
    //Components
	private NavMeshAgent agent;
    private Animator anim;
    //Animation
    /*
    int idleHash = Animator.StringToHash("Base Layer.Idle");
    int runHash = Animator.StringToHash("Base Layer.Run");
    int attackHash = Animator.StringToHash("Base Layer.Attack");
    int damageHash = Animator.StringToHash("Base Layer.Damage");
    */
    AnimatorStateInfo state;
    //Config
    float sightRange = 10.0f;
    float visionFieldWidth = 45.0f; //In Angle
    int patrolWaypointsMax = 7;
    float grabRange = 1.0f;
    //Gameplay
    public GameObject target;
    Waypoint waypointCur;       //Last chosen Waypoint
    bool isPatroling;

    int layerMask;    //Ignore Eenemys, like himself

	AudioSource audio;

//IDLE
    float idleTime;
    float idleTimeMin;
    float idleTimeMax;
//PATROL
	public GameObject[] waypoints;
    ArrayList patrolWaypoints;
    int patrolWaypointsIndex = 0;
    int patrolDirection = -1;   // 1: forward in index <=> -1: backwards in index
    Waypoint waypointPrev;
//States
    public enum AIState
    {
        IDLE,
        PATROL
    }
    AIState currentState;

	public float detectionDelay = 1.5f;
	public float detectionTime;
//-----------------------------------------------------------------------//
                        //DEFAULT METHODS\\
//-----------------------------------------------------------------------//
    void Start () 
	{
        //testPoint.transform.position = this.transform.position;
        //testPoint.transform.position += this.transform.forward;
		audio = GetComponent<AudioSource>();
		InitWaypoints();
		InitScript();
        InitState();
        InitIdle();
        InitPatrol();
    }
	void Update () 
	{
        CheckState();
        DoStateActions();
        //UpdateAnimation();
	}
//-----------------------------------------------------------------------//
                        //Initialization\\
//-----------------------------------------------------------------------//
    void InitScript()
    {
        target = GameManager.Instance.Player();
        //Agent for the Navigation on the NavMesh
        agent = GetComponent<NavMeshAgent>();
        //Get Animator for Animation controls
        anim = GetComponent<Animator>();
        //Layermask for Raycasts (=> ignore Enemiy and Combatlayer)
        layerMask = 1 << LayerMask.NameToLayer("Enemy");    //Ignore Eenemys, like himself
        layerMask = ~layerMask;
		detectionTime = 0;
    }
	void InitWaypoints() {
		waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
	}
    void InitState()
    {
        currentState = AIState.IDLE;
    }
//-----------------------------------------------------------------------//
                        //STATE METHODS\\
//-----------------------------------------------------------------------//
    /**
     * IDLE -> Target in Sight => Start Attacking
     * IDLE -> No Target in Sight AND idle Time is over => Start Freeroaming
     */
    void CheckState()
    {
            switch (currentState)
            {
            case AIState.IDLE:                          //CurrentState: IDLE
                if (idleTime < 0.0f)
                {
                    StartPatrol();
                }
                break;
            case AIState.PATROL:                        //CurrentState: PATROL
                if (WaypointReached())
                {
                    StartIdle();
                }
                break;
            }
    }
    void DoStateActions()
    {
        switch (currentState)
        {
            case AIState.IDLE:
                DoIdle();
                break;
            case AIState.PATROL:
                DoPatrol();
                break;
        }
    }  
//IDLE
    void InitIdle()
    {
        //Time till desciding if staying in IDLE or change to FREEROAMING
        //Always set when entering IDLE state
        idleTime = 0.0f;  
        idleTimeMin = 0.0f; 
        idleTimeMax = 0.0f;
    }
    void StartIdle()
    {
        agent.ResetPath();
        //Debug.Log("State: Idle");
        SetState(AIState.IDLE);
        idleTime = Random.Range(idleTimeMin, idleTimeMax);
    }
    void DoIdle()
    {
        idleTime -= Time.deltaTime;
        CheckVision();
    }
//PATROL
    public void InitPatrol()
    {
        patrolWaypoints = new ArrayList();
        patrolWaypointsIndex = -1;
        isPatroling = false;
        waypointCur = null;
        waypointPrev = null;
        patrolDirection = 0;

    }
    void StartPatrol()
    {
        //Debug.Log("State: Patrol");
        SetState(AIState.PATROL);
        if(waypointCur == null)
        {
            setNewWaypoint(FindNearestWaypoint());
            patrolWaypoints.Add(waypointCur);
            patrolWaypointsIndex = 0;
        }

    }
    void DoPatrol()
    {
        //Debug.Log("WaypointCur: " + waypointCur.name);
        CheckVision();
        //Debug.Log("#PatrolWaypoints: " + patrolWaypoints.Count);
        //Debug.Log("PatrolWaypointsIndex: " + patrolWaypointsIndex);
        if (WaypointReached())
        { 
            if(isPatroling) //A complete set of Patrolpoint Waypoints is already set => Move along the Patrol route
            {
                //Debug.Log("IsPatroling");
                if (patrolWaypointsIndex == 0)  //The Guard is at the Beginning of the Patrol
                {
                    //Debug.Log("Index = 0");
                    patrolDirection = 1;                                                //Change direction of the Patrol, back to the End
                    patrolWaypointsIndex += patrolDirection;                            //Index for the next Patrolpoint to go to
                    setNewWaypoint((Waypoint)patrolWaypoints[patrolWaypointsIndex]);    //Move to next Waypoint of the Patrol
                }
                else if (patrolWaypointsIndex == patrolWaypoints.Count - 1)    //The Guard is at the End of the Patrol
                {
                    //Debug.Log("index = max - 1");
                    patrolDirection = -1;                                               //Change direction of the Patrol, back to the Start
                    patrolWaypointsIndex += patrolDirection;                            //Index for the next Patrolpoint to go to
                    setNewWaypoint((Waypoint)patrolWaypoints[patrolWaypointsIndex]);    //Move to next Waypoint of the Patrol
                }
                else //The Guard is in the middle of the Patrol
                {
                    //Debug.Log("In the middle");
                    patrolWaypointsIndex += patrolDirection;                            //Index for the next Patrolpoint to go to
                    setNewWaypoint((Waypoint)patrolWaypoints[patrolWaypointsIndex]);    //Move to next Waypoint of the Patrol
                }
            }
            else //The Guard is still searching for more Patrolpoints
            {
                AddWaypointToPatrol();
            }
        }
    }
    void ResetPatrol()
    {
        patrolWaypoints.Clear();
        patrolWaypointsIndex = -1;
        //patrolDirection = 0;
        isPatroling = false;
        waypointCur = null;
    }
    void AddWaypointToPatrol()
    {
        /*
        if(waypointPrev != null)
            Debug.Log("WaypointPrev: " + waypointPrev.name);
        else
            Debug.Log("WaypointPrev: null");
            */
        Waypoint nextPatrolWaypoint = waypointCur.GetRandomNeighbour(waypointPrev);
        //Debug.Log("nextPatrolPoint: " + nextPatrolWaypoint);
        waypointPrev = waypointCur;
        setNewWaypoint(nextPatrolWaypoint);
        patrolWaypoints.Add(nextPatrolWaypoint);
        patrolWaypointsIndex = patrolWaypoints.Count - 1;
        if (
            (patrolWaypoints.Count == patrolWaypointsMax)
            //|| (waypointCur.GetNeighboursAmount() <= 1)
            )
        {
            //Debug.Log("patrolWaypointCount: " + patrolWaypoints.Count);
            //Debug.Log("waypointCur.NeighbourCount: " + waypointCur.GetNeighboursAmount());
            isPatroling = true;
        }
    }
    //Helper
    Waypoint FindNearestWaypoint()
    {
        if (waypoints.Length > 0)
        {
			Waypoint nearestWaypoint = waypoints[0].GetComponent<Waypoint>();
            float shortestDistance = DistanceBetween(this.transform.position, waypoints[0].transform.position);
			foreach (GameObject wp in waypoints)
            {
                if (DistanceBetween(this.transform.position, wp.transform.position) < shortestDistance)
                {
					nearestWaypoint = wp.GetComponent<Waypoint>();
                    shortestDistance = DistanceBetween(this.transform.position, wp.transform.position);
                }
            }
            return nearestWaypoint;
        }
        else
        {
            Debug.Log("No Waypoints to choose from");
            return null;
        }
    }
//-----------------------------------------------------------------------//
                        //ACTIONS\\
//-----------------------------------------------------------------------//
    public void CheckVision()
    {
		if (TargetInSight() && PlayerController.Instance().hasOrgan)
        {
			if(!audio.isPlaying) {
				audio.Play();
			}
			detectionTime += Time.deltaTime;
            EventManager.Instance.Event("SUSPICION", "Display", (detectionTime / detectionDelay).ToString());
            //Debug.Log("Guard has Player in Sight");
        } else {
            if (detectionTime != 0)
                EventManager.Instance.Event("SUSPICION", "Display", "0");
            detectionTime = 0;
		}
		if(detectionTime >= detectionDelay) {
			audio.Stop();
			GameManager.Instance.gameState = GameManager.GameStates.OVER;
			EventManager.Instance.Event("LOST","","");
        }
    }
//-----------------------------------------------------------------------//
                        //ANIMATION\\
//-----------------------------------------------------------------------//
    void UpdateAnimation()
    {
        if (agent.velocity.magnitude > 0.2) {
            anim.SetBool ("isWalking", true);
        } 
        else 
        {
            anim.SetBool ("isWalking", false);
        }
    }
//-----------------------------------------------------------------------//
                        //HELPER\\
//-----------------------------------------------------------------------//
    public bool TargetReached()
    {
        //Debug.Log("Target to reach: " + target.name);
        if (target != null)
        {
            RaycastHit hit;
            Vector3 rayOrigin = this.transform.position;
            //rayOrigin.y += addToOriginY;
            //rayOrigin.z += addToOriginZ;
            Vector3 rayDirection = target.transform.position - this.transform.position;
            Ray sight = new Ray(rayOrigin, rayDirection);
            int layerMask = 1 << LayerMask.NameToLayer("Enemy");    //Ignore Eenemys, like himself
            layerMask = ~layerMask;

            if (Physics.Raycast(sight, out hit, grabRange, layerMask))
            {
                if (hit.collider.tag == target.tag)
                {
                    //Debug.Log("Target reached");
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            Debug.Log("No Target to reach!");
            return false;
        }
        return false;
    }
    bool WaypointReached()  
    {
        if (agent.remainingDistance <= 0.05f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    bool TargetInSight()
    {
        if (target != null)
        {
            if ( 
                (DistanceBetween(this.transform.position, target.transform.position) <= sightRange) //Player is in Sightrange
                && (NoEnvironmentBetween(this.transform.position, target.transform.position))       //No Obstacle between the Skeleton and the Player wich is blocking the sight
                && CheckVisionField()                                                               //Player insite Visionfield
                )
            {
                return true;
            }
            return false;
        }
        Debug.Log("No Target to check Sight");
        return false;
    }
    public void setNewWaypoint(Waypoint wp)
    {
        if (wp != null)
        {
            agent.SetDestination(wp.transform.position);
            waypointCur = wp;
        }
        else
        {
            Debug.Log("No Waypoint given for new Waypoint");
/*            Debug.Log("#PatrolWaypoints: " + patrolWaypoints.Count);
            Debug.Log("PatrolWaypointsIndex: " + patrolWaypointsIndex);
            Debug.Log("WaypointCur: " + waypointCur.name);*/
        }
    }
    public int GetRandomNumberFromTo(int min, int max)
    {
        return Random.Range(min, max + 1);
    }
    public bool IsTriggeredWithPercent(int percent)
    {
        int randomNumber = GetRandomNumberFromTo(1, 100);
        if (randomNumber <= percent)
        {
            return true;
        }
        return false;
    }
    public bool NoEnvironmentBetween(Vector3 t1, Vector3 t2)
    {
        LayerMask layerMask = 1 << LayerMask.NameToLayer("Environment");
        Ray ray = new Ray(t1, t2 - t1);
        if (Physics.Raycast(ray, (t2 - t1).magnitude, layerMask))
        {
            return false;
        }
        return true;
    }
    public float DistanceBetween(Vector3 t1, Vector3 t2)
    {
        return (t2 - t1).magnitude;
    }
    public bool CheckVisionField()
    {
        Vector3 toTarget, forward;

        toTarget = target.transform.position - this.transform.position;
        forward = this.transform.forward;

        if (Vector3.Angle(forward, toTarget) <= visionFieldWidth)
            return true;
        else
            return false;
    }
    //Setter
    public void SetState(AIState state)
    {
        currentState = state;
    }
    public void SetSightRange(float range)
    {
        sightRange = range;
    }
    //Getter
    public AIState GetCurrentState()
    {
        return currentState;
    }
    public float GetSightRange()
    {
        return sightRange;
    }
}