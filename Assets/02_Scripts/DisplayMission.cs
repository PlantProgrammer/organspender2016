﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayMission : MonoBehaviour
{
    public Text textContainer;
    public string textToDisplay;

    // Use this for initialization
    void Start()
    {
        EventManager.Instance.RegisterEventListener(
            "MISSION", ProcessMissionEvent);
        textToDisplay = "";
        DisplayText();
    }

    protected void ProcessMissionEvent(string eventType, string param)
    {
        if (eventType == "Info")
            textToDisplay = param;
        else if (eventType == "GameOver")
            textToDisplay = "";
        DisplayText();
    }

    protected void DisplayText()
    {
        textContainer.text = textToDisplay;
        textContainer.gameObject.SetActive(textToDisplay != "");
    }

    protected void Update()
    {
        if (GameManager.Instance.gameState == GameManager.GameStates.RUNNING)
            textContainer.gameObject.SetActive(true);
        else
            textContainer.gameObject.SetActive(false);
    }
}
