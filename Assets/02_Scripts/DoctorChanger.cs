﻿using UnityEngine;
using System.Collections;

public class DoctorChanger : MonoBehaviour {
	public int MeshIndex =0;
	public static DoctorChanger instance;
	public static DoctorChanger Instance {get 
		{
			return instance;
		}
	}

	public GameObject[] doctors;
	// Use this for initialization
	void Awake () {
		instance = this;
	}


	void Start () {
		changeMesh();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void changeMesh() {
		for(int i = 0; i<doctors.Length; i++) {
			doctors[i].SetActive(MeshIndex==i);
		}
	}

	public void changeTo(PatientController.OrganTypes type) {
		switch(type) {
		case PatientController.OrganTypes.HEART:
		case PatientController.OrganTypes.LUNG:
			MeshIndex = 1;
			break;
		case PatientController.OrganTypes.KIDNEY:
			MeshIndex = 2;
			break;
		case PatientController.OrganTypes.LIVER:
			MeshIndex = 3;
			break;
		default:
			MeshIndex = 0;
			break;
		}
		changeMesh();
	}

	public void changeToIndex(int index) {
		MeshIndex = index;
		changeMesh();
	}
}
